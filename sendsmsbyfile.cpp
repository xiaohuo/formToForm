#include "sendsmsbyfile.h"
#include "ui_sendsmsbyfile.h"
#include <QFileDialog>
#include "Python.h"
#include <QDebug>
#include <iostream>
#include <QStringList>
#include <QTime>


SendSMSByFile::SendSMSByFile(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SendSMSByFile)
{
    ui->setupUi(this);

    connect(ui->textBrowser, SIGNAL(cursorPositionChanged()), this, SLOT(autoScroll()));
}

SendSMSByFile::~SendSMSByFile()
{
    delete ui;
}

void SendSMSByFile::on_pushButton_2_clicked()
{
    QString path=QFileDialog::getOpenFileName(this,"选择文件","/home","Text(*.txt *.TXT)");

    qDebug() << path << endl;
    ui->label->setText(path);

    Py_SetPythonHome("d:/Python27");

    Py_Initialize();

    qDebug() << "init done" << endl;
    // 将Python工作路径切换到待调用模块所在目录，一定要保证路径名的正确性
    PyRun_SimpleString("import sys");
//    QString setSysPath = QString("sys.path.append('%1')").arg(QCoreApplication::applicationDirPath());
//    PyRun_SimpleString(setSysPath.toStdString().c_str());
    PyRun_SimpleString("sys.path.append('./')");
    PyRun_SimpleString("sys.path.append('D:/CODE/CPP/formToform')");

    PyObject* pModule = PyImport_ImportModule("ufile_api");
    if(!pModule){
        qDebug() << "import fail" << endl;
    }else{
        qDebug() << "import done" << endl;
    }

    //获取hello函数的指针
     PyObject* pFunhello = PyObject_GetAttrString(pModule,"say_hello");

    //调用函数，传入参数为NULL
     const char *p = path.toStdString().c_str();
//     char *buf = new char[strlen(p)+1];
//     strcpy(buf, p);
     qDebug() << "xx done" << p << endl;
     char *tp = "test";
     PyObject_CallFunction(pFunhello,tp);
    //销毁Python解释器，这是调用的最后一步
     Py_Finalize();

}

void SendSMSByFile::on_sendButton_clicked()
{
    ui->sendButton->setDisabled(true);
    QString fileName = ui->label->text();
    qDebug() << "content:" <<fileName << endl;
    QFile inputFile(fileName);
    QStringList mobileList;
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       while (!in.atEnd())
       {
          QString line = in.readLine();
          qDebug() << "content:" <<line << endl;
          ui->textBrowser->append(line);
          QApplication::processEvents();

          mobileList << line;
       }
       inputFile.close();
    }
    QString res = mobileList.join(",");
    std::string mobis = res.toUtf8().constData();
    std::cout << mobis << "total:" << mobileList.size() << std::endl;
    ui->textBrowser->append(res);
    ui->textBrowser->append(QString("total = %1").arg(mobileList.size()));


    ui->progressBar->setRange(0,5000);

    for(int i =0; i<=5000;i++){
        QString tmp;
        genRandomStr(tmp);
        ui->textBrowser->append(tmp);

        ui->progressBar->setValue(i);
        QApplication::processEvents();

    }

    ui->sendButton->setDisabled(false);

}

void SendSMSByFile::autoScroll(){
    QTextCursor cursor =  ui->textBrowser->textCursor();
    cursor.movePosition(QTextCursor::End);
    ui->textBrowser->setTextCursor(cursor);
}

void SendSMSByFile::genRandomStr(QString &randomStr){
    int max = 16;
    QString tmp = QString("0123456789ABCDEFGHIJKLMNOPQRSTUVWZYZ");
    QString str = QString();
    QTime t;
    t= QTime::currentTime();
    qsrand(t.msec()+t.second()*1000);
    for(int i=0;i<max;i++) {
        int ir = qrand()%tmp.length();
        str[i] = tmp.at(ir);
    }
    randomStr = str;
}
