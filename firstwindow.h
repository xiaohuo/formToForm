#ifndef FIRSTWINDOW_H
#define FIRSTWINDOW_H

#include <QWidget>
#include "sendsmsbyfile.h"

namespace Ui {
class FirstWindow;
}

class FirstWindow : public QWidget
{
    Q_OBJECT

public:
    explicit FirstWindow(QWidget *parent = 0);
    ~FirstWindow();

private slots:
    void on_pushButton_clicked();

private:
    Ui::FirstWindow *ui;
    SendSMSByFile sendSMSByFile;
};

#endif // FIRSTWINDOW_H
