#-------------------------------------------------
#
# Project created by QtCreator 2018-07-12T22:51:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = formToform
TEMPLATE = app


SOURCES += main.cpp\
        firstwindow.cpp \
    sendsmsbyfile.cpp

HEADERS  += firstwindow.h \
    sendsmsbyfile.h

FORMS    += firstwindow.ui \
    sendsmsbyfile.ui

DISTFILES += \
    ufile_api.py


win32: LIBS += -L$$PWD/../../../Python27/libs/ -lpython27

INCLUDEPATH += $$PWD/../../../Python27/include
DEPENDPATH += $$PWD/../../../Python27/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../../Python27/libs/python27.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../../Python27/libs/libpython27.a
