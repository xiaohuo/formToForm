#ifndef SENDSMSBYFILE_H
#define SENDSMSBYFILE_H

#include <QWidget>

namespace Ui {
class SendSMSByFile;
}

class SendSMSByFile : public QWidget
{
    Q_OBJECT

public:
    explicit SendSMSByFile(QWidget *parent = 0);
    ~SendSMSByFile();

    void genRandomStr(QString &randomStr);

private slots:
    void on_pushButton_2_clicked();

    void on_sendButton_clicked();

    void autoScroll();

private:
    Ui::SendSMSByFile *ui;
};

#endif // SENDSMSBYFILE_H
